﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1

{
    public class Node<T>
    {
        public T Data { get; set; }
        public Node<T> PNext { get; set; }
        public Node<T> pPrew { get; set; }
        public Node()
        {
            this.PNext = null;
            this.pPrew = null;

        }
    }

    public class List<T> where T : IComparable 
    {
        private Node<T> Head { get; set; }
        private Node<T> Tail { get; set; }
        private int Size { get; set; }
        
        public List()
        {
            Head = null;
            Tail = Head;
            Size = 0;
        }
        ~List()
        {
            Clear();
        }
        public void Push(T data)
        {
            Node<T> TempNode = new Node<T>();
            TempNode.Data = data;
            if (Head == null)
            {
                Head = TempNode;
                Tail = Head;
            }
            else
            {
                Tail.PNext = TempNode;
                TempNode.pPrew = Tail;
                Tail = TempNode;
            }
            Size++;
        }
        public void Print()
        {
            Node<T> TempNode = new Node<T>();
            TempNode = Head;
            int num = 0;
            while (TempNode != null)
            {
                Console.Write(num + ") ");
                Console.WriteLine(TempNode.Data.ToString());
                TempNode = TempNode.PNext;
                num++;
            }
        }
        public T Search(int num)
        {
            Node<T> TempNode = new Node<T>();
            TempNode = Head;
            for (int i = 0; i < num; i++)
            {
                TempNode = TempNode.PNext;
            }

            return TempNode.Data;
        }
        public void Correct(int num, T data)
        {
            Node<T> TempNode = new Node<T>();
            TempNode = Head;
            for (int i = 0; i < num; i++)
            {
                TempNode = TempNode.PNext;
            }
            TempNode.Data = data;
        }
        public void Pop_back()
        {
            var TempNode = new Node<T>();
            TempNode = Tail.pPrew;
            if (Tail.pPrew != null)
            {
                Tail.pPrew.PNext = null;
            }
            else { Tail = Head = null; }
            Tail = TempNode;
        }
        public void Pop_front()
        {
            var TempNode = new Node<T>();
            TempNode = Head.PNext;
            Head.PNext.pPrew = null;
            Head = TempNode;

        }
        public void Clear()
        {
            Node<T> TempNode = new Node<T>();
            TempNode = Tail;
            while (TempNode != null)
            {
                TempNode = Tail.pPrew;
                Pop_back();

            }
        }
        public void Insert(int index, T data)
        {
            Node<T> TempNode = new Node<T>();
            TempNode = Head;
            var TempNode2 = new Node<T>();
            TempNode2.Data = data;
            for (int i = 0; i < index; i++)
            {
                TempNode = TempNode.PNext;
            }
            TempNode.pPrew.PNext = TempNode2;
            TempNode2.PNext = TempNode;
            TempNode2.pPrew = TempNode.pPrew;
            TempNode.pPrew = TempNode2;
        }
        public void Sort()
        {
            Node<T> TempNode = new Node<T>();
            TempNode =  Head;
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size - 1; j++)
                {
                    if (TempNode.Data.CompareTo(TempNode.PNext.Data) >= 0)
                    {
                        var temp = TempNode.Data;
                        TempNode.Data = TempNode.PNext.Data;
                        TempNode.PNext.Data = temp;
                    }
                    TempNode = TempNode.PNext;
                }
                TempNode = Head;                
            }
        }
    }
}




