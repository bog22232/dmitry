﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> lst = new List<int>();
            lst.Push(5);
            lst.Push(10);
            lst.Push(100);
            lst.Push(5);
            lst.Push(160);
            lst.Push(10067);
            lst.Push(56);
            lst.Push(102);
            lst.Push(0);
            lst.Sort();
            lst.Print();
            Console.ReadKey();
        }
    }
}
